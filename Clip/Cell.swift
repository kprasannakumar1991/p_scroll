//
//  Cell.swift
//  Clip
//
//  Created by prasanna on 3/7/16.
//  Copyright © 2016 mobilitas. All rights reserved.
//

import UIKit

class Cell: UITableViewCell {

    // Mode of the UIImageView MUST be AspectFill for desired effect
    @IBOutlet weak var iconView: UIImageView!
    
    // Constraint between the iconView and TopOfTheCell
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var icon :String? {
        didSet {
            if let icon = icon {
                iconView.image = UIImage(named: icon)
                
                // For new cell, reset the topConstraint
                topConstraint.constant = 0
                self.updateConstraints()
                
            }
        }
    }
    
    func changeTopConstraint(y :CGFloat) {
        print(y)
        topConstraint.constant = y
        self.updateConstraints()

    }
    
    class func height() -> CGFloat {
        return CGFloat(200)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
