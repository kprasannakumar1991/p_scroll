//
//  ViewController.swift
//  Clip
//
//  Created by prasanna on 3/7/16.
//  Copyright © 2016 mobilitas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    let HEIGHT_OFF_CELL = Cell.height()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(200)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! Cell
        
        let index = indexPath.row
        
        if index % 2 == 0 {
            cell.icon = "image2"
        } else {
            cell.icon = "image"
        }
        
        return cell
    }
}

extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView == self.tableView {
            
            // How much the table is scrolled from top
            let yOffSet = self.tableView.contentOffset.y
            
            if yOffSet > 0 && yOffSet < HEIGHT_OFF_CELL {
                
                // Get the first row cell
                let firstIndexPath = NSIndexPath(forRow: 0, inSection: 0)
                let cell = self.tableView.cellForRowAtIndexPath(firstIndexPath) as! Cell
                
                cell.changeTopConstraint(yOffSet)
                
                
            }
        }
    }
}

